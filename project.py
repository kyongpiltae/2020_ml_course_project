import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import FunctionTransformer
from sklearn.pipeline import Pipeline
from sklearn.neighbors import KNeighborsClassifier


def extract_features(X_sample):
    '''
    Extract Features from one sample
    input
        X_sample : (100, 3)
    output
        X_features : (p,)
    '''
    X_fft = np.abs(np.fft.rfft(X_sample, axis=0)) # Fast Fourier Transform (ignore phase)
    mean = np.mean(X_sample, axis=0) # Calculate mean per axis
    std = np.std(X_sample, axis=0)   # Calculate standard deviation per axis
    X_features = np.hstack([mean, std])
    return X_features


def preprocess(X):
    '''
    Preprocess multiple samples
    input
        X : (n, 100, 3)
    output
        X_processed : (n, p)
    '''
    X_processed = np.array([extract_features(X_sample) for X_sample in X])
    return X_processed


class Decomposer(BaseEstimator, TransformerMixin):
    def __init__(self):
        pass
    
    def fit(self, X, *args):
        '''
        Fit decomposer with training data
        '''
        return self
    
    def transform(self, X):
        '''
        Transform input data with fitted decomposer
        '''
        return X[:, [0]]


class KNNClassifierClass: # Example, will not test
    def __init__(self):
        self.decomposer = Decomposer()
        self.clf = KNeighborsClassifier()
    
    def fit(self, X_train, y_train):
        '''
        Fit classifier with training data
        Save fitting information for predict method
        '''
        X_train = preprocess(X_train)
        X_train = self.decomposer.fit(X_train).transform(X_train)
        self.clf.fit(X_train, y_train)
    
    def predict(self, X_test):
        '''
        Predict y with input test data
        Use saved information from fit method
        '''
        X_test = preprocess(X_test)
        X_test = self.decomposer.transform(X_test)
        y_pred = self.clf.predict(X_test)
        return y_pred


def KNNClassifierFunc(): # Example, will not test
    return Pipeline([
        ('preprocessor', FunctionTransformer(preprocess)),
        ('decomposer', Decomposer()),
        ('classifier', KNeighborsClassifier()),
    ], memory='.')

## KNN 예제에서와 같이 LRClassifier, LDAClassifier, RFClassifier, BClassifier, SVMClassifier를 구현해야 한다.